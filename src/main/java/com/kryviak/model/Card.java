package com.kryviak.model;

public class Card {
    private String theme;
    private String type;
    private String country;
    private Integer year;
    private String author;
    private String valuable;

    public Card() {
    }

    public Card(String theme, String type, String country, Integer year, String author, String valuable) {
        this.theme = theme;
        this.type = type;
        this.country = country;
        this.year = year;
        this.author = author;
        this.valuable = valuable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getValuable() {
        return valuable;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }

    @Override
    public String toString() {
        return "Card{" +
                "theme='" + theme + '\'' +
                ", type='" + type + '\'' +
                ", country=" + country +
                ", year='" + year + '\'' +
                ", author=" + author +
                ", valuable=" + valuable +
                '}';
    }
}
