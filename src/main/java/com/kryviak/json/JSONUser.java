package com.kryviak.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kryviak.filepath.FilePath;
import com.kryviak.model.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class JSONUser {
    private static Logger logger = LogManager.getLogger(JSONUser.class);

    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        FilePath filePath = new FilePath();
        Card car = null;
        try {
            car = objectMapper.readValue(new File(filePath.propertyFile("json")), Card.class);
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info(car.toString());
    }
}