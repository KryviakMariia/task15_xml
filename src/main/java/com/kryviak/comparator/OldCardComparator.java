package com.kryviak.comparator;

import com.kryviak.model.Card;

import java.util.Comparator;

public class OldCardComparator implements Comparator<Card> {
    @Override
    public int compare(Card o1, Card o2) {
        return Integer.compare(o1.getYear(), o2.getYear());
    }
}
