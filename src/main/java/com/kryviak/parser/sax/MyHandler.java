package com.kryviak.parser.sax;

import com.kryviak.model.Card;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {

    private List<Card> OldCardList = null;
    private StringBuilder data = null;
    Card card = new Card();

    public List<Card> getOldCardList() {
        return OldCardList;
    }

    private boolean bTheme = false;
    private boolean bType = false;
    private boolean bCountry = false;
    private boolean bYear = false;
    private boolean bAuthor = false;
    private boolean bValuable = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("card")) {
            card = new Card();
            if (OldCardList == null)
                OldCardList = new ArrayList<>();
        } else if (qName.equalsIgnoreCase("theme")) {
            bTheme = true;
        } else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("country")) {
            bCountry = true;
        } else if (qName.equalsIgnoreCase("year")) {
            bYear = true;
        } else if (qName.equalsIgnoreCase("author")) {
            bAuthor = true;
        } else if (qName.equalsIgnoreCase("valuable")) {
            bValuable = true;
        }
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (bTheme) {
            card.setTheme(data.toString());
            bTheme = false;
        } else if (bType) {
            card.setType(data.toString());
            bType = false;
        } else if (bCountry) {
            card.setCountry(data.toString());
            bCountry = false;
        } else if (bYear) {
            card.setYear(Integer.parseInt(data.toString()));
            bYear = false;
        } else if (bAuthor) {
            card.setAuthor(data.toString());
            bAuthor = false;
        } else if (bValuable) {
            card.setValuable(data.toString());
            bValuable = false;
        }

        if (qName.equalsIgnoreCase("card")) {
            OldCardList.add(card);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
