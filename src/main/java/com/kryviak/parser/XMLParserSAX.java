package com.kryviak.parser;

import com.kryviak.model.Card;
import com.kryviak.parser.sax.MyHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XMLParserSAX {
    private static Logger logger = LogManager.getLogger(XMLParserSAX.class);

    public static void main(String[] args) {
        logger.info("SAX");
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            MyHandler handler = new MyHandler();
            saxParser.parse(new File("D:\\projects\\task_15_XML\\src\\main\\resources\\xml\\oldCard.xml"), handler);
            List<Card> empList = handler.getOldCardList();
            for(Card emp : empList)
                logger.info(emp);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
