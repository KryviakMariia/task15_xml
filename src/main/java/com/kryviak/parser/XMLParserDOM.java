package com.kryviak.parser;

import com.kryviak.filepath.FilePath;
import com.kryviak.comparator.OldCardComparator;
import com.kryviak.filechecker.ExtensionChecker;
import com.kryviak.model.Card;
import com.kryviak.parser.dom.DOMParserUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class XMLParserDOM {
    private static Logger logger = LogManager.getLogger(XMLParserDOM.class);

    public static void main(String[] args) {
        FilePath filePath = new FilePath();
        File xml = new File(filePath.propertyFile("xml"));
        File xsd = new File(filePath.propertyFile("xsd"));

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(DOMParserUser.getOldCardList(xml, xsd), "DOM");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Card> oldCardList, String parserName) {
        Collections.sort(oldCardList, new OldCardComparator());
        logger.info(parserName);
        for (Card oldCard : oldCardList) {
            logger.info(oldCard);
        }
    }
}
