package com.kryviak.parser.dom;

import com.kryviak.model.Card;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

class DOMDocReader {
    List<Card> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Card> listOldCards = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("card");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Card card = new Card();

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                card.setTheme(element.getElementsByTagName("theme").item(0).getTextContent());
                card.setType(element.getElementsByTagName("type").item(0).getTextContent());
                card.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                card.setYear(Integer.parseInt(element.getElementsByTagName("year").item(0).getTextContent()));
                card.setAuthor(element.getElementsByTagName("author").item(0).getTextContent());
                card.setValuable(element.getElementsByTagName("valuable").item(0).getTextContent());

                listOldCards.add(card);
            }
        }
        return listOldCards;
    }
}